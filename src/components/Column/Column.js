import { Card } from "..";
import './Column.css'

export function Column(props) {
  const cards = props.cards;
  const renderCards = cards ? cards.map((card) => 
    <Card key={card.id} name={card.name} />  
  ) : null;
  return (
    <div className="column">
      <h1>{props.name}</h1>
      {!props.isCardsHidden  &&
      <ul>
      {renderCards}
      </ul>
    }
    </div>
  );
}

