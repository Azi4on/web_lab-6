export const state = [
  {
    id: 1,
    name: 'TODO',
    cards: [
      { id: 1, name: 'Покормить кота' },
      { id: 2, name: 'Купить хлеб' },
    ],
  },
  {
    id: 2,
    name: 'In progress',
    cards: [{ id: 3, name: 'Сдать лабу' }],
  },
  {
    id: 3,
    name: 'Done',
    cards: [
      { id: 4, name: 'Сходить на лекцию к Даниилу Игоревичу' },
      { id: 5, name: 'Начать верстать на React-е' },
    ],
  },
];
