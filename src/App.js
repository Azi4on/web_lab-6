import React from 'react';
import { state } from './state';
import { Column } from './components';
import './App.css'

function App()  {
  const renderColumns = state.map((column) =>
  <Column key={column.id} name={column.name} cards={column.cards} isCardsHidden={column.isCardsHidden? column.isCardsHidden: false } />
  );
  return (
  <div className="content">
    {renderColumns}
    </div>);
}

export default App;

